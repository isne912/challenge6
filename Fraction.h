#include <iostream>

#ifndef Fraction_h
#define Fraction_h

class Fraction {

public:

	Fraction();
	Fraction(int numerator, int denominator);

	void output();

private:

	int numerator;
	int denominator;

};

Fraction::Fraction() {
	numerator = 0;
	denominator = 0;
}
Fraction(int num, int den) {
	numerator = num;
	denominator = den;

}
void Fraction::output() {
	std::cout << numerator << "/" << denominator;
}
Fraction operator ++(Fraction n1) {
	Fraction n3;
	n3.setnum(n1.number + n1.denom);
	n3.setden(n1.denom);
	return n3;
}
Fraction operator --(Fraction n1) {
	Fraction n3;
	n3.setnum(n1.number - n1.denom);
	n3.setden(n1.denom);
	return n3;
}

#endif
